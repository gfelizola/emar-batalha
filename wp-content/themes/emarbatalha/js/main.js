// Styled Form - by Lagden
;(function($){
    var opts;
    var methods = {
        init: function(options) {
            opts = $.extend({
                'hiddenStyle': 'hiddenStyle'
            }, options);
            return this.each(function(){
                var $this = $(this);
                if ($this.is('select'))
                    methods.styledSelect($this);
            });
        },
        styledSelect: function(self){
            var span = $('<span></span>');
            var options = self.find('option');
            var title = (options.filter(":selected").val() != '') ? options.filter(":selected").text() : options.eq(0).text();
            self
                .after(
                    span
                        .attr("class", self.attr("class"))
                        .css('width', self.width() + 'px')
                        .html(title)
                )
                .addClass(opts.hiddenStyle)
                .bind('change',methods.change);
        },
        change: function(e){
            var span = $(this).next('span:eq(0)').text($('option:selected', this).text());
        },
        destroy: function(){
            return this.each(function(){
                var $this = $(this);
                $this
                    .removeClass(opts.hiddenStyle)
                    .unbind('change')
                    .next('span:eq(0)').remove();
            });
        }
    };
    $.fn.customized = function( method ){
        if ( methods[method] )
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        else if ( typeof method === 'object' || ! method )
            return methods.init.apply( this, arguments );
        else
            $.error( 'Method ' +  method + ' does not exist on jQuery.customized' );
    };
})( jQuery );

function ajustaFooter () {
    //footer
    var alturaFooter = ($(document).height() - $('#content').height() - $('header').height() - 110 );
    // if( $('#wpadminbar').length > 0 ) alturaFooter -= 28 ;
    alturaFooter += 100 ;
    if( alturaFooter < 100 ) alturaFooter = 100 ;
    $('footer').height( alturaFooter + "px" );
    console.log( $(window).height() , $(document).height() );
    if( $(window).height() >= $(document).height() ){
        alturaFooter += 100 ;
        $('footer').height( alturaFooter + "px" );
        $('body,html').css('overflow-y','hidden');
    } else {
        $('body,html').css('overflow-y','auto');
    }
}

jQuery(document).ready(function($) {
    $(window).load(ajustaFooter);
    $(window).resize(ajustaFooter);

    $('body:not(.home) a[href*="/home"]').each(function(i,e) {
        var url = $(this).attr("href");
        if( url.indexOf('?') >= 0 ){
            url += '&' ;
        } else {
            url += '?' ;
        }
        url += 'vd=true';
        $(this).attr("href",url);
    });

    // home
    $('.destaque_principal > div:not(:first)').hide();
    $('.destaque_thumbs ul li:first').hide();
    $('.destaque_thumbs ul li:visible:eq(2)').addClass('no-border');
    $('.destaque_thumbs ul li a').click(function(e) {
        e = e || event;
        e.preventDefault();
        var idx = $(this).parent('li').index();
        $('.destaque_principal > div:visible').hide();
        $('.destaque_principal > div:eq(' + idx + ')').fadeIn();
        $('.destaque_thumbs ul li').removeClass('no-border').show();
        $(this).parent('li').hide();
        $('.destaque_thumbs ul li:visible:eq(2)').addClass('no-border');
    });
    var currentDestaqueHome = 0;
    $('.home a.seta').click(function(e) {
        e.preventDefault();
        if ($(this).hasClass('direita')) {
            currentDestaqueHome++
            if( currentDestaqueHome >= $('.destaque_thumbs ul li').length ) currentDestaqueHome = 0;
            $('.destaque_thumbs ul li:eq(' + currentDestaqueHome + ') a').click();
        } else {
            currentDestaqueHome--
            if( currentDestaqueHome < 0 ) currentDestaqueHome = $('.destaque_thumbs ul li').length - 1;
            $('.destaque_thumbs ul li:eq(' + currentDestaqueHome + ') a').click();
        }
    });
    createGrid('.lista-items');
    $('.galeria').PikaChoose({ carousel: true, autoPlay: false, showCaption: false });
    $('.combo-categorias a.atual').click(function(e) {
        e = e || event ;
        e.preventDefault();
        $('.combo-categorias ul.items').toggle();
    });

    $(".js-boxinfo").fancybox({
        width     : 830,
        height    : 500,
        padding   : 0,
        margin    : 0,
        closeBtn  : false,
        closeClick: false,
        afterShow : function(){
            // Custom Form
            $('select.Styled').customized();
            $("#form-informacoes").validate({
                rules: {
                    "boxinfo[titulo]": "required",
                    "boxinfo[nome]": "required",
                    "boxinfo[sobrenome]": "required",
                    "boxinfo[email]": {
                        required: true,
                        email: true
                    },
                    "boxinfo[pais]": "required"
                },
                messages: {
                    "boxinfo[titulo]": "Por favor, escolha um título",
                    "boxinfo[nome]": "Por favor, informe seu nome",
                    "boxinfo[sobrenome]": "Por favor, informe seu sobrenome",
                    "boxinfo[email]": {
                        required: "Por favor, informe seu e-mail",
                        email: "Por favor, informe um e-mail válido"
                    },
                    "boxinfo[pais]": "Por favor, informe seu país"
                },
                errorLabelContainer: $('div.errors-container'),
            });
        }
    });

    if( $(".boxinfo.novidade").length ) {
        $.fancybox( $(".boxinfo.novidade"), {
            width     : 830,
            height    : 500,
            padding   : 0,
            margin    : 0,
            closeBtn  : false,
            closeClick: false,
        });
    }

    if ( $(".modal_video.intro").length ) {
        $.fancybox( $(".modal_video.intro"), {
            maxWidth    : 800,
            maxHeight   : 600,
            padding     : 5,
            width       : '70%',
            height      : '70%'
        });
    };

});
function createGrid (container) {
    if( $(container).hasClass('imprensa') ){
        $('.grid-item', container).addClass('col1').addClass('row1');
    } else {
        $('.grid-item', container).each(function (i,e) {
            switch(i){
                case 0:
                    $(this).addClass('col1').addClass('row1');
                    break;
                case 1:
                    $(this).addClass('col2').addClass('row1');
                    break;
                case 2:
                    $(this).addClass('col1').addClass('row1').addClass('omega');
                    break;
                case 3:
                    $(this).addClass('col1').addClass('row2');
                    break;
                case 4:
                    $(this).addClass('col2').addClass('row1');
                    break;
                case 5:
                    $(this).addClass('col1').addClass('row2').addClass('omega');
                    break;
                case 6:
                    $(this).addClass('col1').addClass('row1');
                    break;
                case 7:
                    $(this).addClass('col1').addClass('row1');
                    break;
            }
        });
}
}