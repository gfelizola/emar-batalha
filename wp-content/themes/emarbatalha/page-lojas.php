<?php
$background = get_field('imagem_de_fundo');
$repeticao 	= get_field('repeticao_do_fundo');
$posicao 	= get_field('posicao_do_fundo');
$cor_menu 	= get_field('header_bgcolor');

get_header();

$lojas = new WP_Query(array(
	'post_type' => 'lojas',
	'posts_per_page' => -1,
	'orderby' => 'post_title'
));
?>
<div id="contentwrap">
	<div id="content" class="lojas">
		<ul>
		<?php
		$cc = 0;
		if ($lojas->have_posts()) : while ($lojas->have_posts()) : $lojas->the_post();
			echo $cc % 2 == 0 ? "<li class='media'>" : "<li class='media tr'>";

			$imagem   = get_field( 'imagem_loja' );
			$titulo   = the_title(NULL, NULL, false);
			$local    = get_field( 'local' );
			$endereco = get_field( 'endereco' );
			$bairro   = get_field( 'bairro' );
			$cidade   = get_field( 'cidade' );
			$estado   = get_field( 'estado' );
			$telefone = get_field( 'telefone' );

			$class = $cc % 2 == 0 ? "alignleft" : "alignright";

			$titulo = mb_strtoupper($titulo);

			if( $imagem ) 	echo '<img class="' . $class . '" title="loja" alt="" src="' . $imagem['url'] . '" width="226" />';
							echo "<h1>$titulo</h1>";
			if( $local ) 	echo "<h2>" . mb_strtoupper($local) . "</h2>";
							echo "<p>";
			if( $endereco ) echo "$endereco<br>";
			if( $bairro ) 	echo "$bairro<br>";
			if( $cidade ) 	echo $cidade;
			if( $estado ) 	echo " - $estado<br>";
			if( $telefone ) echo $telefone;
							echo "</p>";

							echo "</li>" . "\n";
			$cc++
		?>
		<?php endwhile; endif; ?>
		</ul>
	</div>
</div>
<?php get_footer(); ?>