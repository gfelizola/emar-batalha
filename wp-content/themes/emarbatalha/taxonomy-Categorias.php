<?php
get_header();

$colecoes_ids = array();
?>
<div id="contentwrap">
	<div id="content" class="lista-items categorias">

		<div class="paginacao">
			<?php
			global $wp_query;

			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$categoria_atual = (get_query_var('Categorias')) ? get_query_var('Categorias') : false;

			$posts_por_tax = array();
			$termos = get_terms( 'colecoes', 'orderby=id&order=DESC&hide_empty=1' );

			foreach ($termos as $termo ) {
				$tax_args = array(
					'posts_per_page' => 1,
					'order' => 'ASC',
					'tax_query' => array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'colecoes',
							'field' => 'id',
							'terms' => $termo->term_id
						),
						array(
							'taxonomy' => 'Categorias',
							'field' => 'slug',
							'terms' => $categoria_atual
						)
					)
				);

				query_posts($tax_args);

				while (have_posts()) {
					$tem_post = true ;
					the_post();

					$imagem = get_field('imagem_do_produto') ;

					$posts_por_tax[] = array(
						'id' => $post->ID,
						'data' => $post->post_date,
						'imagem' => $imagem['sizes']['medium'],
						'link' => get_permalink(get_the_ID()),
						'titulo' => get_the_title()
					);
				}
			}

			function sort_on_data($a,$b){
				if ($a['data'] == $b['data']) {
					return 0;
				}
				return ($a['data'] < $b['data']) ? -1 : 1;
			}

			usort($posts_por_tax, "sort_on_data");

			$qtde_pagina = 8;
			$total_termos = count( $posts_por_tax );
			$total_paginas = ceil( $total_termos / $qtde_pagina );

			$page_url = site_url() . "/Categorias/$categoria_atual/page/%d/" ;

			echo "<div class='wp-pagenavi'>";
			if( $paged > 1 ){
				$proxima_pagina = $paged - 1 ;
				echo sprintf("<a href='$page_url' class='previouspostslink'>»</a>", $proxima_pagina);
			}
			echo "<span class='pages'>$paged de $total_paginas</span>";
			if( $paged < $total_paginas ){
				$proxima_pagina = $paged + 1 ;
				echo sprintf("<a href='$page_url' class='nextpostslink'>»</a>", $proxima_pagina);
			}

			echo "</div>";
			?>
		</div>

		<div class="combo-categorias">
			<a href="#" class="atual">Categorias</a>
			<ul class="items">
				<?php
				$tax_name = 'Categorias' ;
				$args = array(
					'hide_empty' 	=> 0,
					'orderby' 		=> 'name'
				);

				$categorias = get_terms($tax_name, $args);

				foreach ($categorias as $cat) {
					?>
					<li><a href="<?php echo site_url($tax_name . '/' . $cat->slug) ?>"><span><?php echo $cat->name ?></span></a></li>
					<?php
				}
				?>
			</ul>
		</div>

		<div class="produtos-container">
			<?php

			$pagina_mat = $paged - 1 ;
			$inicial = $qtde_pagina * $pagina_mat ;
			$fim = ($qtde_pagina * $pagina_mat) + $qtde_pagina - 1 ;

			if( $fim > $total_termos ) $fim = $total_termos;

			$cc = 0 ;

			foreach ($posts_por_tax as $post_atual ) {
				$tem_post = false ;

				if ($cc >= $inicial && $cc <= $fim ) {

					?>
					<div class="produto grid-item" id="post-<?php echo $post_atual['id']; ?>" style="background-image:url('<?php echo $post_atual['imagem']; ?>')">
						<a href="<?php echo $post_atual['link']; ?>"><span><?php echo $post_atual['titulo']; ?></span></a>
					</div>
					<?php
				}

				$cc++;
			}?>
		</div>
	</div>
</div>
<?php get_footer(); ?>