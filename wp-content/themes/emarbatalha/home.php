<?php get_header('blog'); ?>
<div id="contentwrap">
	<div id="content">
		<?php if (have_posts()) : ?>
			<?php $post = $posts[0]; ?>
			<?php while (have_posts()) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" class="post">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="link permanente para <?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<cite>
						<span class="date">Postedo em: <abbr title="<?php the_time('l, d F Y, H:i') ?>" class="published"><?php the_time('l, d F Y, H:i') ?></abbr></span>
					</cite>
					
					<?php the_content('Continuar lendo &raquo;'); ?>
					
					<!-- footer class="post-info">
						<?php edit_post_link('Moderar','','|'); ?>
						<?php comments_popup_link('Nenhum Comentário &#187;', '1 Comentário &#187;', '% Comentários &#187;'); ?>
					</footer -->
				</div>
				<!-- POST // end -->
			<?php endwhile; ?>
			<nav>
				<div class="previous"><?php next_posts_link('&laquo; Posts anteriores') ?></div>
				<div class="next"><?php previous_posts_link('Próximos posts &raquo;') ?></div>
			</nav>
			<!-- POSTS // end -->
		<?php else : ?>
			<!-- NO POSTS  // woops -->
			<div>
				<h2>Nenhum Post(s) encontrado</h2>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php get_footer('blog'); ?>
