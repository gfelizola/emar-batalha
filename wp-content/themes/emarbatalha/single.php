<?php get_header('blog'); ?>
<div class="blog">
    <div id="contentwrap">
        <div id="content">
        <?php if (have_posts()) : the_post(); ?>
        <!-- POST // begin -->
        <div id="post-<?php the_ID(); ?>" class="post">
            <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
            <cite>
               <span class="date">Postedo em: <abbr title="<?php the_time('Y-d-m\TG:i:s') ?>" class="published"><?php the_time('jS F Y') ?></abbr></span>
               <span class="author">Por <?php the_author() ?></span>
           </cite>
           <?php the_content('Continue reading &raquo;'); ?>

           <nav>
              <div class="previous"><?php previous_post_link('&laquo; %link'); ?></div>
              <div class="next"><?php next_post_link('%link &raquo;') ?></div>
          </nav>

          <footer class="post-info">
            <small>Categoria: <?php the_category(', ') ?> | <?php the_tags( 'Tags: ', ', ', ''); ?>
                 | <?php post_comments_feed_link('RSS (destes comentários)'); ?> |

                <?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) { ?>
                <!-- Both Comments and Pings are open -->
                <a href="#respond">Deixe um comentário</a>

                <?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) { ?>
                <!-- Only Pings are Open -->
                Comentários fechados.

                <?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) { ?>
                <!-- Comments are open, Pings are not -->
                Trackbacks disabilitados.

                <?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) { ?>
                <!-- Neither Comments, nor Pings are open -->
                Comentários fechados e Trackbacks disabilitados.

                <?php } edit_post_link('Moderar','| ',''); ?></small>

                <?php wp_link_pages(array('before' => '<p><strong>Páginas:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
            </footer>

            <div id="comments">
               <?php comments_template(); ?>
           </div>
       </div>

    <?php else : ?>

     <!-- NO POSTS  // woops -->
    <div>
        <h2>Nenhum Post(s) encontrado</h2>
    </div>

    <?php endif; ?>

        </div>
    </div>
</div>

<?php get_footer('blog'); ?>
