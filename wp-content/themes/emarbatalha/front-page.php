<?php 
$background = get_field('imagem_de_fundo');
$repeticao 	= get_field('repeticao_do_fundo');
$posicao 	= get_field('posicao_do_fundo');
$cor_menu 	= get_field('header_bgcolor');
?>
<?php get_header(); ?>
<div id="contentwrap">
	<div id="content" class="intro">
		<?php 
		if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>
</div>
<?php get_footer(); ?>