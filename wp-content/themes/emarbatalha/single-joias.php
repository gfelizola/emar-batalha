<?php
get_header();



?>
<div id="contentwrap">
	<div id="content" class="produtos">
		<?php while (have_posts()) : the_post(); ?>
			<div class="produto" id="post-<?php the_ID(); ?>">
				<div class="imagem">
					<?php $imagem = get_field('imagem_do_produto') ?>
					<a class="js-boxinfo fancybox.ajax" href="<?php bloginfo('url'); ?>/informacao?produto=<?php the_ID(); ?>">
						<img src="<?php echo $imagem['sizes']['large']; ?>" alt="" />
					</a>
				</div>
				<div class="descricao">
					<?php
					$categorias = get_the_terms( get_the_ID(), 'Categorias');
					$colecoes 	= get_the_terms( get_the_ID(), 'colecoes');

					$s_cat="";
					$s_col="";

					echo "<h3>";
					if( $categorias ){
						foreach ($categorias as $cat) {
							$s_cat = $cat->name;
							echo $cat->name . '<br>';
						}
					}
					echo "<br>";
					if ( $colecoes) {
						foreach ($colecoes as $col) {
							$s_col = $col->name;
							echo $col->name . '<br>';
						}
					}
					echo "</h3>";
					?>
					<?php the_content() ?>
					<p><?php the_field('referencia') ?></p>
				</div>
				<div class="shareit">
					<?php $subMail = "Indicação de Emmar Batalha" ?>
					<?php $bodyMail = "Indicação do seguinte produto: {$s_cat} - {$s_col} | ref.: " . get_field('referencia') . " - " . get_permalink() ?>
					<a target="_blank" href="mailto:?subject=<?php echo urlencode($subMail) ?>&amp;body=<?php echo urlencode($bodyMail) ?>" rel="nofollow">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_indica.png" alt="">
						<span>Indique para uma amiga(o)</span>
					</a>
					<a class="js-boxinfo fancybox.ajax" href="<?php bloginfo('url'); ?>/informacao?produto=<?php echo get_the_ID(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_emar.png" alt="">
						<span>Mais informações</span>
					</a>
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink()); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_facebook.png" alt="">
						<span>Facebook</span>
					</a>
				</div>
				<?php
				$categoria_atual = null;
				if( $categorias ){
					$categoria_atual = array_shift( $categorias );
					?>
					<a href="<?php echo site_url() . '/' .$categoria_atual->taxonomy .'/'. $categoria_atual->slug ?>/" class="bt-voltar">voltar</a>
					<?php

				}

				if ( $colecoes && $categoria_atual !== null ) {
					$colecao_atual = array_shift( $colecoes );

					$args_ant_prox_msm = array(
						'post_type' => 'joias',
						'posts_per_page' => 1,
						'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'Categorias',
								'field' => 'id',
								'terms' => $categoria_atual->term_id
							),
							array(
								'taxonomy' => 'colecoes',
								'field' => 'id',
								'terms' => $colecao_atual->term_id
							)
						),
						'post__not_in' => array($post->ID)
					);

					function filter_where_proximo( $where = '' ) {
						global $wpdb,$post;
						$where .= " AND {$wpdb->posts}.post_date < '{$post->post_date}' AND {$wpdb->posts}.ID != {$post->ID}";
						return $where;
					}

					function filter_where_anterior( $where = '' ) {
						global $wpdb,$post;
						$where .= " AND {$wpdb->posts}.post_date > '{$post->post_date}' AND {$wpdb->posts}.ID != {$post->ID}";
						return $where;
					}

					add_filter( 'posts_where', 'filter_where_proximo' );
					$query_proximo = new WP_Query( $args_ant_prox_msm );
					remove_filter( 'posts_where', 'filter_where_proximo' );

					add_filter( 'posts_where', 'filter_where_anterior' );
					$query_anterior = new WP_Query( array_merge($args_ant_prox_msm, array('orderby' => 'date', 'order' => 'ASC' )));
					remove_filter( 'posts_where', 'filter_where_anterior' );

					if( $query_anterior->posts[0] !== null ){
						echo "<a href='" . get_permalink($query_anterior->posts[0]->ID) . "' class='seta direita'>Próxima Jóia</a>";
					}

					if( $query_proximo->posts[0] !== null ){
						echo "<a href='" . get_permalink($query_proximo->posts[0]->ID) . "' class='seta esquerda'>Jóia anterior</a>";
					}

					$args_rel = array(
						'post_type' => 'joias',
						'posts_per_page' => 6,
						'order' => 'ASC',
						'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'Categorias',
								'field' => 'id',
								'terms' => $categoria_atual->term_id
							),
							array(
								'taxonomy' => 'colecoes',
								'field' => 'id',
								'terms' => $colecao_atual->term_id
							)
						),
						'post__not_in' => array($post->ID)
					);

					$relacionados = new WP_Query($args_rel);
				}
				?>
			</div>


		<?php endwhile;?>
		<?php if( $relacionados ){
			if ($relacionados->have_posts()){
			?>
		<div class="relacionados">
			<div class="navegacao clearfix">
				<div class="left">relacionados</div>
				<div class="right"></div>
			</div>
			<ul>
			<?php
			while ($relacionados->have_posts()) : $relacionados->the_post(); ?>
			<?php $imagem = get_field('imagem_do_produto') ?>
			<li style="background-image:url('<?php echo $imagem['sizes']['thumbnail']; ?>');"><a href="<?php echo get_permalink( get_the_ID()) ?>" class="ir">
				<?php the_title() ?>
			</a></li>
			<?php endwhile; ?>
			</ul>
		</div>
	</div>
	<?php }
	} ?>
</div>
<?php get_footer(); ?>