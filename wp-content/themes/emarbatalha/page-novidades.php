<?php
$background = get_field('imagem_de_fundo');
$repeticao 	= get_field('repeticao_do_fundo');
$posicao 	= get_field('posicao_do_fundo');
$cor_menu 	= get_field('header_bgcolor');

get_header();

$url_lancamentos = get_permalink();
$args = array(
	'post_type'      => 'joias',
	'posts_per_page' => 1,
	'meta_key'       => 'lançamento',
	'meta_value'     => '1'
);
$atual = $_GET['p'];
if( $atual ) $args['name'] = $atual ;
$lancamentos = new WP_Query($args);
$relacionados = new WP_Query(array(
	'post_type'      => 'joias',
	'posts_per_page' => 6,
	'meta_key'       => 'lançamento',
	'meta_value'     => '1',
	'post__not_in'   => array($lancamentos->posts[0]->ID)
));
?>
<div id="contentwrap">
	<div id="content" class="produtos">
		<?php
		if ($lancamentos->have_posts()) : while ($lancamentos->have_posts()) : $lancamentos->the_post(); ?>
			<div class="produto" id="post-<?php the_ID(); ?>">
				<div class="imagem">
					<?php $imagem = get_field('imagem_do_produto') ?>
					<a class="js-boxinfo fancybox.ajax" href="<?php bloginfo('url'); ?>/informacao?produto=<?php echo $lancamentos->posts[0]->ID; ?>">
						<img src="<?php echo $imagem['sizes']['large']; ?>" alt="" />
					</a>
				</div>
				<div class="descricao">
					<?php
					$categorias = get_the_terms( get_the_ID(), 'Categorias');
					$colecoes 	= get_the_terms( get_the_ID(), 'colecoes');
					$s_cat="";
					$s_col="";
					echo "<h3>";
					if( $categorias ){
						foreach ($categorias as $cat) {
							$s_cat = $cat->name;
							echo $cat->name . '<br>';
						}
					}
					echo "<br>";
					if ( $colecoes) {
						foreach ($colecoes as $col) {
							$s_col = $col->name;
							echo $col->name . '<br>';
						}
					}
					echo "</h3>";
					?>
					<?php the_content() ?>
					<p><?php the_field('referencia') ?></p>
				</div>
				<div class="shareit">
					<?php $subMail = "Indicação de Emmar Batalha" ?>
					<?php $bodyMail = "Indicação do seguinte produto: {$s_cat} - {$s_col} | ref.: " . get_field('referencia') . " - " . get_permalink() ?>
					<a target="_blank" href="mailto:?subject=<?php echo urlencode($subMail) ?>&amp;body=<?php echo urlencode($bodyMail) ?>" rel="nofollow">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_indica.png" alt="">
						<span>Indique para uma amiga(o)</span>
					</a>
					<a class="js-boxinfo fancybox.ajax" href="<?php bloginfo('url'); ?>/informacao?produto=<?php echo $lancamentos->posts[0]->ID; ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_emar.png" alt="">
						<span>Mais informações</span>
					</a>
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink()); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_facebook.png" alt="">
						<span>Facebook</span>
					</a>
				</div>
				<?php
				$proxima = get_adjacent_post_plus( array('in_same_meta' => 'lançamento' ), false );
				$anterior = get_adjacent_post_plus(  array('in_same_meta' => 'lançamento' ) );

				if( count($proxima) ) echo "<a href='$url_lancamentos?p=" . $proxima[0]->post_name . "' class='seta esquerda'>Próxima Jóia</a>";
				if( count($anterior) ) echo "<a href='$url_lancamentos?p=" . $anterior[0]->post_name . "' class='seta direita'>Jóia anterior</a>";
				?>
			</div>
		<?php endwhile; endif; ?>

		<div class="relacionados">
			<div class="navegacao clearfix">
				<div class="left"></div>
				<div class="right"></div>
			</div>
			<ul>
			<?php
			if ($relacionados->have_posts()) : while ($relacionados->have_posts()) : $relacionados->the_post(); ?>
			<?php $imagem = get_field('imagem_do_produto') ; ?>
			<li style="background-image:url('<?php echo $imagem['sizes']['thumbnail']; ?>');"><a href="<?php echo "$url_lancamentos?p=" . $post->post_name; ?>" class="ir">
				<?php the_title() ?>
			</a></li>
			<?php endwhile; endif; ?>
			</ul>
		</div>
	</div>
</div>
<?php
if ( $atual == "" ) {
$img = get_field('imagem_novidades','options');
?>
<div class="boxinfo novidade">

	<div class="boxinfo-header clearfix">
		<div>
			<a href="javascript:jQuery.fancybox.close();">fechar X</a>
		</div>
	</div>

	<div class="boxinfo-content">
		<div class="boxinfo-texto">
			<h3><?php the_field('titulo_novidades','options') ?></h3>
			<?php the_field('conteudo_novidades','options') ?>
		</div>

		<div class="boxinfo-imagem">
			<img src="<?php echo $img['sizes']['large']; ?>" alt="" />
		</div>
	</div>
</div>
<?php
}
get_footer();
?>
