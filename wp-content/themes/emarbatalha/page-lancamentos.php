<?php 
$background = get_field('imagem_de_fundo');
$repeticao 	= get_field('repeticao_do_fundo');
$posicao 	= get_field('posicao_do_fundo');
$cor_menu 	= get_field('header_bgcolor');

get_header(); 

$lancamentos = new WP_Query(array(
	'post_type' => 'joias', 
	'posts_per_page' => 1,
	'meta_key' => 'lançamento', 
	'meta_value' => '1'
));

$relacionados = new WP_Query(array(
	'post_type' => 'joias', 
	'posts_per_page' => 6,
	'meta_key' => 'lançamento', 
	'meta_value' => '1'
));
?>
<div id="contentwrap">
	<div id="content" class="produtos">
		<?php 
		if ($lancamentos->have_posts()) : while ($lancamentos->have_posts()) : $lancamentos->the_post(); ?>
			<div class="produto" id="post-<?php the_ID(); ?>">
				<div class="imagem">
					<?php $imagem = get_field('imagem_do_produto') ?>
					<a class="js-boxinfo fancybox.ajax" href="<?php bloginfo('url'); ?>/informacao?produto=<?php echo $lancamentos->posts[0]->ID; ?>">
						<img src="<?php echo $imagem['sizes']['large']; ?>" alt="" />
					</a>
				</div>
				<div class="descricao">
					<?php 
					$categorias = get_the_terms( get_the_ID(), 'Categorias');
					$colecoes 	= get_the_terms( get_the_ID(), 'colecoes');

					$s_cat="";
					$s_col="";

					echo "<h3>";
					if( $categorias ){
						foreach ($categorias as $cat) {
							$s_cat = $cat->name;
							echo $cat->name . '<br>';
						}
					}
					echo "<br>";
					if ( $colecoes) {
						foreach ($colecoes as $col) {
							$s_col = $col->name;
							echo $col->name . '<br>';
						}
					}
					echo "</h3>";
					?>
					<p><?php the_field('referencia') ?></p>
				</div>
				<div class="shareit">
					<?php $subMail = "Indicação de Emmar Batalha" ?>
					<?php $bodyMail = "Indicação do seguinte produto: {$s_cat} - {$s_col} | ref.: " . get_field('referencia') . " - " . get_permalink() ?>
					<a target="_blank" href="mailto:?subject=<?php echo urlencode($subMail) ?>&amp;body=<?php echo urlencode($bodyMail) ?>" rel="nofollow">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_indica.png" alt="">
						<span>Indique para uma amiga(o)</span>
					</a>
					<a class="js-boxinfo fancybox.ajax" href="<?php bloginfo('url'); ?>/informacao?produto=<?php echo $lancamentos->posts[0]->ID; ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_emar.png" alt="">
						<span>Mais informações</span>
					</a>
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink()); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ico_facebook.png" alt="">
						<span>Facebook</span>
					</a>
				</div>
				<?php if($relacionados->have_posts()){
					echo "<a href='" . get_permalink($relacionados->posts[0]) . "' class='seta direita'>Próxima Jóia</a>";
					if ($relacionados->post_count > 1) {
						echo "<a href='" . get_permalink($relacionados->posts[$relacionados->post_count-1]) . "' class='seta esquerda'>Jóia anterior</a>";
					}
				} ?>
			</div>
		<?php endwhile; endif; ?>
	
		<div class="relacionados">
			<div class="navegacao clearfix">
				<div class="left"></div>
				<div class="right"></div>
			</div>
			<ul>
			<?php 
			if ($relacionados->have_posts()) : while ($relacionados->have_posts()) : $relacionados->the_post(); ?>
			<?php $imagem = get_field('imagem_do_produto') ?>
			<li><a href="<?php echo get_permalink( get_the_ID()) ?>" style="background-image:url('<?php echo $imagem['sizes']['thumbnail']; ?>');" class="ir">
				<?php the_title() ?>
			</a></li>
			<?php endwhile; endif; ?>
			</ul>
		</div>
	</div>
</div>
<?php get_footer(); ?>